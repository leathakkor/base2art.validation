﻿namespace Base2art.Validation
{
    public class ObjectMatcher<T> : IObjectMatcher<T>
        where T : class
    {
        private readonly IObjectMatcher<T> objectMatcher;

        private readonly T value;

        public ObjectMatcher(T value)
        {
            this.value = value;
        }

        public ObjectMatcher(IObjectMatcher<T> objectMatcher)
        {
            this.objectMatcher = objectMatcher;
        }

        public T Value
        {
            get
            {
                if (this.objectMatcher != null)
                {
                    return this.objectMatcher.Value;
                }

                return this.value;
            }
        }
    }
}