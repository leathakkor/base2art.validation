﻿namespace Base2art.Validation
{
    public interface IMatcher<out T>
    {
        T Value { get; }
    }
}