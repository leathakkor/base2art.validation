﻿namespace Base2art.Validation
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1040:AvoidEmptyInterfaces", Justification = "SjY")]
    public interface IObjectMatcher<out T> : IMatcher<T>
        where T : class
    {
    }
}