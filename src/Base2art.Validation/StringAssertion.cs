﻿namespace Base2art.Validation
{
    using System;

    public static class StringAssertion
    {
        public static IObjectMatcher<string> IsNotNullEmptyOrWhiteSpace(this IMatcher<string> value)
        {
            value.Validate().IsNotNull();
            if (string.IsNullOrWhiteSpace(value.Value))
            {
// ReSharper disable NotResolvedInText
                throw new ArgumentNullException("value");
// ReSharper restore NotResolvedInText
            }

            return new ObjectMatcher<string>(value.Value);
        }
    }
}