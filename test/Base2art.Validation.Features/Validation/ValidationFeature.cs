﻿namespace Base2art.Validation
{
    using System;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ValidationFeature
    {
        [Test]
        public void ShouldBeAbleToValidateString()
        {
            this.ShouldThrow<ArgumentNullException>(() => ((string)null).Validate().IsNotNull());
            string.Empty.Validate().IsNotNull();

            this.ShouldThrow<ArgumentNullException>(() => ((string)null).Validate().IsNotNullEmptyOrWhiteSpace());
            this.ShouldThrow<ArgumentNullException>(() => string.Empty.Validate().IsNotNullEmptyOrWhiteSpace());
            this.ShouldThrow<ArgumentNullException>(() => "  ".Validate().IsNotNullEmptyOrWhiteSpace());
            " sdf ".Validate().IsNotNull().And().IsNotNullEmptyOrWhiteSpace();

            " sdf ".Validate().IsNotNull().And(x => x.Length).GreaterThan(3);
            this.ShouldThrow<ArgumentOutOfRangeException>(() => " sdf ".Validate().IsNotNull().And(x => x.Length).GreaterThan(5));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => " sdf ".Validate().Is("sdf"));
            "sdf".Validate().Is("sdf");
        }

        [Test]
        public void ShouldBeAbleToValidateComparable()
        {
            const int X = 3;
            X.Validate().GreaterThan(2).And().LessThan(4);
            X.Validate().Is(3);

            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().Is(4));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().GreaterThan(4));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().LessThan(2));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().GreaterThan(3));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().LessThan(3));



            X.Validate().GreaterThanOrEqualTo(3).And().LessThanOrEqualTo(3);
            X.Validate().Is(3);

            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().Is(4));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().GreaterThanOrEqualTo(4));
            this.ShouldThrow<ArgumentOutOfRangeException>(() => X.Validate().LessThanOrEqualTo(2));
        }


        [Test]
        public void ShouldBeAbleToValidateType()
        {
            this.ShouldThrow<ArgumentException>(() => ((string)null).Validate().IsA<string>());
            new Person().Validate().IsA<Person>();

            this.ShouldThrow<ArgumentException>(() => new Person().Validate().IsA<Employee>());
            new Employee().Validate().IsA<Person>();
        }


        [Test]
        public void ShouldBeAbleToValidateNotDefault()
        {
            this.ShouldThrow<ArgumentException>(() => (0).Validate().IsNotDefault());
            1.Validate().IsNotDefault();
            
            this.ShouldThrow<ArgumentException>(() => (1).Validate().IsNot(1));
            (1).Validate().IsNot(2);
            (2).Validate().IsNot(1);
            
            this.ShouldThrow<ArgumentException>(() => ("d").Validate().IsNot("d"));
            this.ShouldThrow<ArgumentException>(() => ("").Validate().IsNot(string.Empty));
            "d".Validate().IsNot("");
            "".Validate().IsNot("d");
            "d".Validate().IsNot("c");
            "c".Validate().IsNot("d");
            
            this.ShouldThrow<ArgumentException>(() => ("d").Validate().NotEqualTo("d"));
            this.ShouldThrow<ArgumentException>(() => ("").Validate().NotEqualTo(string.Empty));
            "d".Validate().NotEqualTo("");
            "".Validate().NotEqualTo("d");
            "d".Validate().NotEqualTo("c");
            "c".Validate().NotEqualTo("d");
            
//            this.ShouldThrow<ArgumentException>(() => ((Person)null).Validate().NotEqualTo(null));
//            new Employee().Validate().IsA<Person>();
        }


        [Test]
        public void ShouldHaveValue()
        {
            this.ShouldThrow<ArgumentException>(() => (0).Validate().HasValue());
            1.Validate().HasValue();
            
            this.ShouldThrow<ArgumentException>(() => ((string)null).Validate().HasValue());
            this.ShouldThrow<ArgumentException>(() => (string.Empty).Validate().HasValue());
            "h".Validate().HasValue();
        }

        private void ShouldThrow<T>(Action act)
            where T : Exception
        {
            act.ShouldThrow<T>();
        }

        private class Person
        {
        }

        private class Employee : Person
        {
        }
    }
}
